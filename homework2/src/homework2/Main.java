package homework2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
		
	static boolean done = false;

	public static void main(String argv[]) throws IOException {
		
		final String filter = argv[0];
		final String PROGRAM = "git log --pretty=format:%H --grep=" + filter;
		
		BufferedReader is;  // reader for output of process
	    String line;

	    final Process p = Runtime.getRuntime().exec(PROGRAM);
	    is = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    while (!done && ((line = is.readLine()) != null))
	    	System.out.println(line);
	    
		for (int i = 0; i < 10; i++) {
			System.out.println(i+2);
		}
	    // getInputStream gives an Input stream connected to
	    // the process p's standard output (and vice versa). We use
	    // that to construct a BufferedReader so we can readLine() it.
	    
	    return;
	}

}
